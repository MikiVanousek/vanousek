---
title: This Is Why Universities Should Make Free and Open Source Contributions Part Of the Curriculum
date: 2022-09-29
tags: ["foss"]
---

As part of higher education studies, students complete many projects.
I attend University of Twente, which prioritizes project work in its teaching methodology.
I applaud its Challenge Based Learning initiative, which aims to focus students projects are real issues faced by people and companies.

I suggest, that at University of Twente as well as in other universities more attention should be directed towards contribution to free and open source software (**FOSS**) -- community developed software free for anyone to use, inspect, modify and redistribute.

There are plentiful FOSS projects with vibrant communities happy to accept contributions and offering their feedback and leadership. 
The opportunities are vast and available for students of many majors -- not just programmers!
FOSS projects are also looking for help in UX & graphical design, hardware design, user testing, marketing, translations, etc.
These projects are products, and they are looking for help in every part of their creation process.

Student FOSS contribution is realistic, and the potential benefits are diverse. It has been tried before.
It is challenging, but the students benefit from the experience.<cite>[^1]</cite>

Contributing to an existing project requires the student to navigate the existing codebase, read its code, learn its structure and adapt to it.
These skills are vital in most jobs graduates take and can not be learned by making greenfield projects.

Improving FOSS is in the public interest.
The fact FOSS has users means contributions generate value.
This can be a great source of motivation and purpose.
This effect is strongest if the student makes a contribution to a product they themselves use.

Engagement of a university with FOSS can improve its public image.
The institution will rise in contributors' and users' eyes.
While FOSS is not mainstream, the community consist in big part of potential students, teachers, researchers, etc. 
Furthermore, reputation is contagious and can start a positive feedback loop of attracting more talent, which increases reputation. 

The proposed project work would of course also have some disadvantages.
As each new contribution is unique, it is more labor-intensive to assign the contribution to an existing project.<cite>[^2]</cite>
Grading of such projects would due to their individual nature be more subjective. 
But these are, to a lesser extent, problems of project work in general.
Grading is much easier and more objective with the sterile exams.
Project work tries to emulate real world challenges to teach students the skills they truly need in their careers.
FOSS contributions take it one step further -- the challenges are real. 
This amplifies the drawbacks as well as the benefits.

[^1]: [Engaging Students in Open Source: Establishing FOSS Development at a University](https://www.researchgate.net/publication/331113706_Engaging_Students_in_Open_Source_Establishing_FOSS_Development_at_a_University)

[^2]: [Training Software Engineers Using Open-Source Software: The Professors' Perspective](https://www.researchgate.net/publication/319552273_Training_Software_Engineers_Using_Open-Source_Software_The_Professors'_Perspective)
