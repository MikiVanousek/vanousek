---
title: Miki's Values
draft: true
---

- your values are what you do, not what you say
- must be controllable, in your power

- Each person must never be treated only as a means to some other end, but must also be treated as an end themselves. - Emanuel Kant

## The Goals = The Axioms
- Health
- Wealth
- Relationships
- Wisdom