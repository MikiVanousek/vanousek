---
title: Consumers Fail To Protect Their Privacy, Perhaps The Government Should Try
date: 2022-03-12
showTableOfContents: true
tags: ["foss"]
---

## Introduction

Privacy is a human right many people have been denied under oppressive government regimes.
But in the last decades, the vast majority of people living in liberal democracies have been giving up much of their privacy rights in exchange for free internet services.
The government should protect the consumers' personal data because the data being collected is used to efficiently manipulate us with ads, the average consumer does not understand the severity of the issue, and opting out of data collection is extremely costly in today's technology market. In this essay, I aim to explain why data is being collected, point out the lack of awareness in the general public, show how market pressure forces companies to violate our privacy and argue the need for governmental intervention.

## Data is being collected, because personalized marketing is effective

People feel uncomfortable with the amount of data, that is being collected about them.<cite>[^1]</cite>
So why do companies still do it?
One study found personalized ads to be twice as effective as traditional marketing.<cite>[^1]</cite>
Furthermore, consumers consistently report being frustrated with irrelevant ads.<cite>[^1]</cite>
Personalization brings value to them as well.
It is then no wonder all major advertising agencies massively collect our personal information, and many of the largest companies on the market today -- Alphabet, Meta, Microsoft and Amazon amongst the top 10 -- engage heavily in mass data collection and personalized advertising.

## The average person does not understand the severity of the issue

According to a Pew Research study, about 13% of Americans tend to read the entire privacy policy before agreeing to it. Furthermore, 74% of Americans admit they usually do not read privacy policies before agreeing at all.
Most people also report understanding 'very little' to 'nothing' about the ways their personal information is handled by corporations and the government.<cite>[^2]</cite>
"One study revealed that when a law that required websites to inform visitors of covert tracking started to be enforced in the Netherlands, in 2013, advertisement click-through rates dropped.
Controlled experiments have found similar results."<cite>[^1]</cite>
This shows people care about their privacy, but are mostly unaware of the extent to which data about them is being collected.
This is not surprising.
It is impossible for anyone to be an expert on everything, let alone everyone.

## But if the consumers value their privacy, why do they keep using privacy-intrusive services?

Because opting out is extremely costly in today's technology market.
We are asked to agree to privacy regularly -- a quarter of people reported being asked almost daily.
Furthermore, most people say they feel it impossible not to sign away their privacy rights.<cite>[^2]</cite>
Consenting to invasive privacy policies is then often done not by choice, but from necessity.
Companies will grow or cease based on the profits they make.
Since collecting user data is a profitable business, companies will engage in it or go out of business.

## The government needs to step in and protect our privacy rights

Since we can not understand everything ourselves, we rely on government to listen to experts and keep us safe.
We do not have to check every grocery for toxic chemicals.
Consumers trust the regulations to keep the market clear of polluted 'drinking' water, unsafe vehicles and pills with dangerous side effects.
We do not have the capacity to scrutinize every product we use.
Users lack the time to read the privacy policies.
It should be ensured by our governments, they are not overly intrusive.
The Harvard lawyer Lewis Rice argues freedom and regulation are equivalent.<cite>[^3]</cite>
If you want to enjoy a freedom, there needs to be a regulation that restricts another person from infringing your freedom.
Our privacy is a freedom worth protecting.

## Conclusion

Privacy is a human right many people throughout history did not have the privileged to enjoy. 
Today, much of the internet infrastructure is paid by selling private personal information.
While users consent to the information being collected,
they often do so oblivious to the scale of the harvesting and wider implications of their actions.
Companies abuse the ignorance of most users and will continue doing so to maximize profits.
Therefore, the government needs to protect citizens from these practices with regulation.


[^1]: John, L. K., Kim, T., & Barasz, K. (2017, December 21).
Targeting Ads Without Creeping Out Your Customers. Harvard Business Review.
https://hbr.org/2018/01/ads-that-dont-overstep

[^2]: Auxier, B., Rainie, L., Anderson, M., Perrin, A., Kumar, M., & Turner, E. (2020, August 17).
Americans and Privacy: Concerned, Confused and Feeling Lack of Control Over Their Personal Information.
Pew Research Center: Internet, Science & Tech.
https://www.pewresearch.org/internet/2019/11/15/americans-and-privacy-concerned-confused-and-feeling-lack-of-control-over-their-personal-information/

[^3]: Rice, L. (2015, October 6). Freedom Is Just Another Word for . . . Regulation. Harvard Law Today. 
https://today.law.harvard.edu/book-review/freedom-is-just-another-word-for-regulation/