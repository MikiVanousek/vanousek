---
title: Pool Day
date: 2023-06-10
---
It's pool day and not everyone is invited. But I am.
Twenty pushups to get the extra pump, and I am walking through the metal rotating fence.

The pool has two planks from which you can jump, 1.5 and 5 meters respectively.
My sister used to dive competitively, and we always jumped cliffs together any opportunity our family vacations have afforded us. 

But this is college pool, not pool with family. The jumps are a benchmark of your dexterity. They will place you on a social hierarchy. 
At the campus of a technical university, the competition is fierce.

An attractive male returns to his female. His jumps are simple, but elegant. This allowed him to defend his place in the social hierarchy. For his effort, he receives a short, but sweet kiss. His female is satisfied with her matching. 

The water can only hurt you, if you are afraid to get your ass slapped.
But your peers are watching. "Ouch! Are you ok? Wow, your back is so red."

When I was 14 I saw a video on Facebook: A boy scout friend of mine did a perfect running backflip from a cliff at least 4 meters high. This jump, also known as reverse rotation, has been on my to-do list ever since. I wanted to be that guy. Six years later, today, I have done it.