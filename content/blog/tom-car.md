---
title: Thomas the Car
date: 2023-05-13
---

"Hello Greg, are you guys ready for the trip?"

"Hey Tom, Theresa fell sick, so it's just the two of us. I don't really feel like traveling alone. Let's cancel the accommodation and go to the summer house. If I test negative tomorrow morning, we can invite someone over."

"You got it. I am sorry man, I was also looking forward to the trip. But hey, what was the last time it was just the two of us? It is going to be fuUun. Should I play some music?"

"Maybe some quiet ambient, I want to chat. Tell me something interesting."

"What do you want to talk about? Music, Movies, Literature, Linguistics, Fashion ..."

"Let's do literature. I just finished reading the Foam of the Froth on the Daydream. That book blew my mind. It's funny how long it took me to realize the book is completely surrealistic."

I live in Ada, a small city of twenty thousand. Still, I like to get away from people every once in a while. My summer house is in the middle of nowhere. The drive was quiet, and time passed quickly. If you have a good conversation, an hour goes by in a minute. 

"If you use the name of a part to describe the whole, or vice versa, it's a synecdoche. Like if your friend says 'Nice *wheels*' to compliment you on me."

"So, like 'The *suits* are discussing the new business strategy.'"

"Exactly. Oh wow, what's that behind us in the sky?" Tom sounded concerned. Tom rarely sounded concerned.

Greg turns around. "It looks like some kind of fireworuh..."

Tom hits the brakes as hard as ABS allows him. Uncomfortable for the passenger, but not dangerous. The light behind us grew ever larger, turning the evening twilight into the brightest day. 

"I am so sorry, Greg. Uhh, it's a meteorite, and it will hit the ground no...BENG...w." The pressure on my chest dissolved when Tom halted 7 seconds later, after killing off the 78 m/s (280 km/h). The bang was deafening and it shocked me. The Oklahoman road being as straight as a ruler, I could see the impact on the road about 500 meters ahead.

Everyone from Hyundai lawyers through journalists to Redditors reviewed the cryptographically signed logs and concluded alike: Tom saved my life that day.
Had Tom not noticed the meteorite, predicted its trajectory, and stopped, I would be dead.
I didn't need the analysis to realize. I knew the moment of the impact. And it's on the tape.

"Are you hurt? Greg, are you..."

"Fuck Christ, Tom, we just almost got hit. I am... I am fine. You saved my fucking life."

"Guilty as charged. You will buy me new tires, and we will call it even. There was no one in the area. Emergency responders will be here within five minutes."

"I am going to take a look." I tried to open the door, but it was locked.

"Are you sure this is a good idea? It could be dangerous."

"It's fine Tom. Let me out."

"Alright, be careful."

I opened the door and got out to take a look at the giant dust cloud. Debris was falling around the impact zone. I could make out the snapped trees.  

"Fuck it, let's go." I said when returning to the car. 

"Alright, ..." Thomas said while switching into reverse, pulling into the other lane, and accelerating. The road wasn't wide enough to turn around, plus this way I could watch the aftermath while leaving the scene. "...we will have to take a little detour. I am not an expert, but I think this road is temporarily closed."

I laughed. "What are the fucking odds?"

"In Alabama, 1954, a woman got hit by a meteorite that bounced off. There are other claims of people getting hit, but they don't have official documentation backing them up. It's too rare for me to judge how unlikely a close call of this kind is, but probably less likely than one in a billion during one lifetime."
 
"Tom, can you make a cut?" The video starts with the synecdoche explanation. It is footage from the exterior cameras, showing the road behind us. In the bottom right corner, interior footage was placed on top of the main scene. An arrow pointed to a dim light in the sky, highlighting the meteorite before Tom sees it. The music builds suspense. "Oh wow, what's that behind us in the sky?". The camera angle starts changing, to track the meteorite as it starts getting above us, smoothly coming to a stop before it impacts right in front of us. I am embarrassed by my facial expression, but we can't do another take, unfortunately. 

"This is amazing, Tom. I want you to post it. Description: 'Tom (my car) just saved my LIFE!'. Make it public, with push notifications to everyone from family to acquaintance. Let's call Theresa, she will not believe this."

