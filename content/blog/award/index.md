---
title: Just As Advertised
date: 2023-04-01
cover: congrats.png
coverAlt: You did it boy!
---

On the 28 November 2022 I officially made it.
My academic achievements in the first year of university have been recognized -- I received an award!
The Dutch Royal Society of Humanities and Sciences threw a ceremony for us in their little palace.
I shook hands, I took pictures, and I received a diploma.
I ate sandwiches with the crust cut off and drank coffee from porcelain, while making a 'personal connection' with the sponsor of our prize.

I joined the big boys on the other side of life.
I officially made it -- I achieved success.
As you might expect, success and happiness go hand in hand. 
And I do not mean the fleeing happiness you might get when eating a cake or having great sex.
This is the deep sense of satisfaction you get when you won the comparison to your peers based on grades
-- one of the mainstream's measure of success.

I have the award for over three months now.
My life is fundamentally different, just short of perpetual feeling of ecstasy.
My life will never again be the stream of problems and complications, the endless to-do list.
Just as expected. Just as advertised.
