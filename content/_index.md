---
---

Hey, I am Miki -- a student of Math and CS in the Netherlands from Czechia. Welcome to my website! I'm really happy you stopped by.

Take a look at my blog, my podcast or Horolux - the app I developed!
