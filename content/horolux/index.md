---
layout: simple
title: "Let Me Introduce You To Horolux"
description: Learn about Horolux, an app that imporves your sleep 
summary: Learn about Horolux, an app that imporves your sleep 
---

## The Problem You Have

You wake up in a dark room. Should you get up? It is impossible to tell.
You need to turn around and reach for your phone to check the time - this wakes you up even more.
![A man who woke up even more](wake-up.jpeg "From [Pexels](https://www.pexels.com/photo/a-man-lying-on-the-bed-while-using-his-mobile-phone-6943426/)")

## Horolux Is The Solution

Horolux is an [open source](https://gitlab.com/MikiVanousek/horolux) app turns the display<cite>[^1]</cite> of your phone, tablet or computer into a digital sun - it gradually lights up as the morning approaches.
If you wake up at any point of the night, the amount of light in the room will allow you to instantly tell if you can keep on sleeping.

<table>
  <tr>
    <td>
{{< figure
    src="settings-sc.png"
  >}}
    </td>
    <td>
{{< figure
    src="night-sc.png"
  >}}
    </td>
    <td>
{{< figure
    src="middle-sc.png"
  >}}
    </td>
    <td>
{{< figure
    src="wake-sc.png"
  >}}
    </td>
  </tr>
</table>

Just set a time you need to get up and the duration for which the display lights up!

{{<alert>}}
Horolux **does not replace your alarm**. It merely compliments its functionality.
{{</alert>}}

Horolux is in the open testing phase [on Google Play](https://play.google.com/store/apps/details?id=com.vanousek.horolux). You can also give it a go in [your browser](https://horolux.vanousek.com).

[^1]: The best result can archived with an OLED display.
