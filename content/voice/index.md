---
layout: simple
title: The Voice Of Miki Vanoušek
---
{{< podcast-links >}}

Hey, if you like my website or my personality, I think you would enjoy the podcast I make. I talk there to the fascinating people I had the opportunity to meet about their passions and life stories.

![An image of me recording an episode](pc-headshot.jpg)