---
showTableOfContents: true
title: ICS Notes
---

- Scripted actions

## Study plan

1. prime from text book - first sentence from every section
2. lecture
3. read text book
4. mind map during daily review
5. explain to imaginary student during weekly review
6. (create) test questions during monthly review

## Fundamentals
### Note taking
- delay the note taking
- use visual note taking (mind maps)

## SIR
### Spacing

- Daily, weekly and monthly revives
- Schedule important things at safe times!
- Schedule important, not urgent things early in the day!

### Interleaving

- Never study the same material twice the same way!

#### Interleaving Techniques
- [x] doodle
- [x] mind map
- [x] explain to imaginary student
- [x] answer practice questions
- [ ] create test questions (real world context)
- [ ] use a study group to quiz and debate


### Retrieval

- Never note things, before thinking about them and understand them!
- Delay writing notes!
- Use memory before checking notes, even when that means you will make a mistake!
- Practice in full, without shortcuts!

## Marginal gains
### 1. Anchor the goal

Decrease the time I spend studying to 35 focused hours a week by Christmas 2023, while noticeably increasing my retention level.

### 2. Dissect the goal

Skills:
1. Resisting distractions (7)
  - no over-stimulation before dinner
  - Leach Block - time bound on PC, eternal on phone
  - focus mode on phone
  - do not disturb mode while studying
2. Deep focus (6)
  - practice meditation
  - forest focus blocks
3. Creating and following a schedule (5)
  - weekend planing
  - scheduling breaks
  - checking in
4. Sleep (8)
  - respecting light and temperature
  - regular bedtime and wake-up time
  - supplements
  - practicing sports
  - naturally sleep good
5. Reading (4)
  - reading slow
6. Understanding concepts (7)
  - delayed note-taking
  - active questioning of relationships

### Tracking
#### 1. week
1. I started noting down distractions and added some more restrictions.
2. I am getting back into the habit of deep focus tracking.
3. I moved back to google calendar and reorganized my schedule. I follow it more closely.
4. Nothing. I started drinking caffeine, which might be detrimental to my sleep.
5. I am trying pre-reading - reading the first sentence of each paragraph.
6. I am learning about delayed mind maps. I used priming before a lecture.
