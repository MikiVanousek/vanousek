---
title: Personal Growth & Well Being Coaching
---

Areas of improvement:
  - Health & Well-Being
  - Focus & Productivity1

Weekly meetings will provide you with:
  - Accountability
  - My experience
  
## Plan
1. Come up with 1-3 (SMART) goals you want to work on. They should be 3 months to 1 year in the future.
2.  